import json
import argparse
import math
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc as scipy_misc
import matplotlib.image as mpimg


# Function to calculate angle between two points
def angle_between(p1, p2):
    x = p1[0] - p2[0]
    y = p1[1] - p2[1]
    alpha = math.degrees(math.atan2(y,x))
    return (alpha + 360) % 360

######################################################################
# GUI Class
#
class Environment(object):
    def __init__(self, x, y, reflect_coeff):
        # initalise an array of size x,y to represent our blank environment
        self.x = x
        self.y = y
        self.map = np.zeros((x, y))
        self.mask_orientation = None
        self.mask_adjacents = None
        self.reflection_points = {}
        self.reflected = np.zeros((x, y))
        self.reflectionCoef = reflect_coeff
        

    def place_source(self, init_power, source_coord):
        x, y = source_coord
        x = int(x)
        y = int(y)
        reflect_angles = {}

        # Verify validity of placed source
        assert x <= self.map.shape[0]
        assert y <= self.map.shape[1]

        # Obtain angles between source and object, for blocks paths
        blocked_angles = set()
        blocked_dists = {}
        for index, value in np.ndenumerate(self.obj):
            if value != 0:
                curr_x, curr_y = index
                angle = angle_between((curr_x, curr_y), (x, y))
                delta = (abs(curr_x - x), abs(curr_y - y))
                distance = math.ceil(math.sqrt(delta[0]**2 + delta[1]**2))

                # Angle +- 1 (Rounding for lower resolutions)
                for new_angle in [int(angle)+1, int(angle), int(angle)-1]:
                    if new_angle in blocked_angles:
                        old_distance = blocked_dists[new_angle]
                        new_distance = min(old_distance, distance)
                    else:
                        new_distance = distance
                    # Add Angle and Update Distance
                    blocked_angles.add(new_angle)
                    blocked_dists.update({new_angle:new_distance})
                    

        # Start to compute signal strength values for all array elements
        for index, value in np.ndenumerate(self.map):
            curr_x, curr_y = index
            delta = (abs(curr_x-x), abs(curr_y-y))
            angle = angle_between((curr_x,curr_y),(x,y))
            distance = math.sqrt(delta[0] ** 2 + delta[1] ** 2)
            reflect_angles[(curr_x,curr_y)] = int(angle)

            # Check if the current location has been blocked (Approximation)
            high_int_angle = math.ceil(angle)
            low_int_angle = math.floor(angle)
            if high_int_angle in blocked_angles and math.floor(distance) > blocked_dists[high_int_angle]:
                if low_int_angle in blocked_angles and math.floor(distance) > blocked_dists[low_int_angle]:
                    continue

            # Check if the current location has been blocked (Extreme Verify)
#            print index
#            newX = curr_x
#            newY = curr_y
#            gradient = math.tan(math.radians(angle))
#            blocked = False
#            # Get the xranges to walk along
#            walk_x = range(newX, x)
#            if len(walk_x) == 0:
#                walk_x = range(newX, x, -1)
#            for newX in walk_x:
#                if newX >= self.x or newX < 0:
#                    break
#                # Find next point
#                destY = int(gradient*(newX - x) + y)
#
#                walk_y = range(newY, destY)
#                if len(walk_y) == 0:
#                    walk_y = range(newY, destY, -1)
#                for newY in walk_y:
#                    if newY >= self.y or newY < 0:
#                        break
#                    # Check if mask
#                    if self.obj[newX,newY] > 0:
#                        blocked = True
#                if blocked:
#                    break
#            if blocked:
#                continue
            
            # Check if the current location is part of the solid mask (wall)
            if self.obj[index] == 0:
                received_power = self.loss_function(init_power, coord_delta=delta)
                self.map[index] = max(self.map[index], received_power)
                # Add to reflection array
                point_orient = self.mask_adjacents[(curr_x, curr_y)]
                if point_orient > 0:
                    self.reflection_points[(curr_x, curr_y)] = (angle, received_power, point_orient)
            else:
                self.map[index] = 0

    # Use all the previously recorded reflection point to compute the
    # signal strength fo the reflected points.
    def perform_reflections(self):
        for x,y in self.reflection_points:
            angle, input_power, orientation = self.reflection_points[(x,y)]
            if (orientation == 100): #Horizontal
                if(angle >= 0 and angle < 90):
                    outputAngle = 180 - angle
                if (angle >= 90 and angle < 180):
                    outputAngle = 180 - angle
                if (angle >= 180 and angle < 270):
                    outputAngle = 540 - angle
                if (angle >= 270 and angle < 360):
                    outputAngle = 540 - angle
            if (orientation == 200): #Vertical
                if (angle >= 0 and angle < 90):
                    outputAngle = 360 - angle
                if (angle >= 90 and angle < 180):
                    outputAngle = 360 - angle
                if (angle >= 180 and angle < 270):
                    outputAngle = 360 - angle
                if (angle >= 270 and angle < 360):
                    outputAngle = 360 - angle
            if(outputAngle < 0):
                outputAngle += 360
            if(outputAngle > 360):
                outputAngle -= 360

            outputPower = input_power * self.reflectionCoef
            newX = x #Start at our reflection point and go either left or right
            newY = y
            gradient = math.tan(math.radians(outputAngle))
            if (outputAngle < 180): #Iterate through x going right
                while newX < self.x:
                    # Find next point
                    newX += 1
                    newY = int(gradient*(newX - x) + y)
                    #print newX, newY
                    if (newX >= self.x or newY >= self.y or newY < 0):
                        break

                    # Check if mask
                    if self.obj[newX,newY] > 0:
                        break

                    # Not a mask, therefore compute the power for this square
                    received_power = self.loss_function(init_power = outputPower, coord_delta = [(newX-x),(newY-y)])
                    self.map[newX,newY] = max(self.map[newX,newY],received_power)
                    self.reflected[newX,newY] = 100

            else: #Iterate through x going left
                while newX > 0 and self.obj[newX,newY] > 0:
                    newX -= 1
                    newY = int(gradient*(newX - x) + y)

                    if (newX < 0 or newY >= self.y or newY < 0):
                        break

                    if self.obj[newX, newY] > 0:
                        break

                    # Not a mask, therefore compute the power for this square
                    received_power = self.loss_function(init_power=outputPower, coord_delta=[(newX - x), (newY - y)])
                    self.map[newX, newY] = max(self.map[newX, newY], received_power)
                    self.reflected[newX, newY] = 100
                    


    def loss_function(self, init_power, distance=None, coord_delta=None, wavelength=None):
        # Parameter input check
        if distance is None and coord_delta is None:
            return None
        elif len(coord_delta) != 2:
            return None

        if wavelength is None:
            wavelength = 0.003 # 3mm default

        # If given coordinates instead of a ecludian distance, compute it.
        if distance is None:
            x, y = coord_delta
            distance = math.sqrt(x**2 + y**2)

        # Scale distance
        distance = distance/100.0

        # Perform the loss function code now that we have init_power and dist

        # Placeholder linear power decay function
        if distance == 0:
            return init_power
        else:
            #return init_power - distance
            #print (4.0*math.pi*distance)/wavelength
            final_power = init_power - 20.0*math.log(((4.0*math.pi*distance)/wavelength),10)
            if final_power < 25:
                return final_power #0
            else:
                return final_power

    def visualise(self, ax):
        plt.close('all')
        ax = plt.imshow(self.map - self.obj)
        return ax

    def add_object(self, obj):
        self.obj = np.zeros((self.x,self.y))
        for i in range(40,85):
            for j in range(70,80):
                self.obj[i,j] = 100
        return self.obj
    
    def add_mask(self, mask_file=None, mask_arr=None):
        # Check valid inputs
        if mask_file is None and mask_arr is None:
            return None

        # Image Mask Importing Code
        def import_mask(map_size_x, map_size_y, img_path):
            # Import the image and resize it
            img = mpimg.imread(img_path)
            # Resize/Interpolate the image
            img1 = scipy_misc.imresize(img, (map_size_x,map_size_x))
            # Flatten the image from RGB to jsut one value
            if len(img1.shape) > 2:
                img1 = img1[:,:,0]*img1[:,:,1]*img1[:,:,2]
            # Make the values binary (black and white)
            img1[img1 < 250] = 100    # Black
            img1[img1 >= 250] = 0
            
            # Plot the mask
            fig, ax = plt.subplots(1, 1, figsize=(8, 8))
            plt.imshow(img1)
            plt.title('Map Mask')
            # Visualise
            plt.show()
            # The ask Array
            return img1
        
        if mask_arr is None:
            mask_arr = import_mask(self.x, self.y, mask_file)
        else:
            assert mask_arr.shape[0] == self.x
            assert mask_arr.shape[1] == self.y
        
        self.obj = mask_arr

        def detect_orientation(mask):
            mask_orientation = np.zeros((self.x, self.y))
            for index, value in np.ndenumerate(mask):
                if value > 0:
                    x, y = index
                    H = False
                    V = False
                    if x+1 < self.x and x-1 >= 0:
                        if mask[x+1, y] > 0 and mask[x-1, y] > 0:
                            V = True
                    if y+1 < self.y and y-1 >= 0:
                        if mask[x, y+1] > 0 and mask[x, y-1] > 0:
                            H = True

                    if H and V:
                        continue
                    elif H:
                        mask_orientation[x, y] = 100
                    elif V:
                        mask_orientation[x, y] = 200
                    else:
                        continue
                else:
                    continue
            return mask_orientation

        def create_adjacents(mask, mask_orientation):
            adjacent_set = np.zeros((self.x, self.y))
            for index, value in np.ndenumerate(mask_orientation):
                # If reflective surface
                if value > 0:
                    x, y = index
                    # Look at adjacent squares
                    if x+1 < self.x and x-1 >= 0:
                        if mask[x+1, y] == 0:
                            adjacent_set[(x+1, y)] = mask_orientation[(x, y)]
                        if mask[x-1, y] == 0:
                            adjacent_set[(x-1, y)] = mask_orientation[(x, y)]
                    if y+1 < self.y and y-1 >= 0:
                        if mask[x, y+1] == 0:
                            adjacent_set[(x, y+1)] = mask_orientation[(x, y)]
                        if mask[x, y-1] == 0:
                            adjacent_set[(x, y-1)] = mask_orientation[(x, y)]
            # print adjacent_set
            return adjacent_set

        self.mask_orientation = detect_orientation(self.obj)
        self.mask_adjacents = create_adjacents(self.obj, self.mask_orientation)

        return self.obj

######################################################################
# Main Function, how to use main function with arg parser
#
# def main(args):
#     print 'Parsing data from' + args.file
#     parse_file(args.file)
######################################################################
# Main Funcction that will be executed whence python file is run.
#
######################################################################
if __name__ == '__main__':
    ##################################################################
    # User the command line argument to open the correct config file
    ##################################################################
    parser = argparse.ArgumentParser(
        description='Millimeter Wave Reflections')
    parser.add_argument('-f', '--file', default='config.json',
        help='The image to be used as a mask')
    
    args = parser.parse_args()

    
    ##################################################################
    # Retrieve the scheduler settings from configuration file
    # One can have multiple files setup for scripting reasons
    # E.g. Change simulation profiles
    ##################################################################
    # Currently reading from a file, but can be retrieved from a url
    with open(args.file) as config_file:
        config = json.load(config_file)

    # Unpack the configuration settings from our schedRR JSON
    # the JSON is for variable readability,
    # but in code the variables are shorter for coding ease
    our_config = config['settings']
    x = our_config['vert_size']
    y = our_config['hor_size']
    mask_file = our_config['file_name']
    source_list = our_config['source_placements']
    loss_function = our_config['loss_function']
    reflect_coeff = our_config['reflection_coefficient']


    print our_config


    our_map = Environment(x, y, reflect_coeff)

    #Make an object that can't be penetrated
    #obj = fromimage
    #our_map.add_object(0)
    our_map.add_mask(mask_file=mask_file)

    #Place the source at the end
    for source_params in source_list:
        power = source_params['source_power']
        x_pos = source_params['vert_position']
        y_pos = source_params['hor_position']
        our_map.place_source(power, (x_pos*x,y_pos*y))

    # Perform the reflection after all the sources have been added
    our_map.perform_reflections()

    # Visualise the resulting environment
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    our_map.visualise(ax)

    plt.savefig('environment.pdf')
    plt.show()

    
    
