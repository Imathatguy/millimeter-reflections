import numpy as np
import matplotlib.pyplot as plt
import pylab

[antennaX, antennaY] = [np.random.rand(1), np.random.rand(1)]

colors = np.random.rand(1)
area = np.pi * (85 * np.random.rand(1))**2  # 0 to 85 point radii

pylab.ylim([0,1])
pylab.xlim([0,1])
plt.scatter(antennaX, antennaY, s=area, c=colors, alpha=0.5)
plt.show()